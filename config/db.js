const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/expensetracker', { useNewUrlParser: true });

module.exports = mongoose;