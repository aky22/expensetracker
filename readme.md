# **Költségvetés nyilvántartó program**

## **Rövid leírás**

Az alkalmazás lehetőséget ad követni a mindennapi pénzügyeidet. Be lehet állítani a fizetésed összegét, amit minden fizetésnapon egy gomb megnyomásával hozzá lehet adni a rendelkezésre álló pénzösszegedhez.  
Ezen kívül egyszeri jövedelmet is fel lehet venni.

A kiadásokat is fel lehet vinni egyesével, A kiadásokat negatív előjellel tárolja a rendszer azáltal kiszámolva a rendelkezésre álló pénzösszeget

## **Entitások**

### Users

| ID | Username | Password |
| ---|:--------:| -------:|
||||

A felhasználó kezelésért felelős enitás. Csak autentikációra lesz használva, illetve a felhasználókhoz kapcsolódik a Transactions entitás, ami által számon lehet tartani, hogy melyik felhasználóhoz melyik entitás tartozik.

### Transactions

Ennek az entitásnak a feladata, hogy nyilvántartsa a bevételeket és a kiadások. 


| ID | Date | Amount | Comment | User_ID |
| --- | --- | ------ | ------- | ------- | 
||||||

Két féle tranzakciót lehet felvenni, ami negatív amountal kerül mentésre az az expense, ami pedig pozitívval az az income.

Az a datok MongoDB-ben fogom tárolni.

## **Mockupok**

![Bejelentkező képernyő](https://confluence.colonier.games/download/attachments/983095/Login.png?version=1&modificationDate=1569656552835&api=v2)

![Dashboard](https://confluence.colonier.games/download/attachments/983095/Dashboard.png?version=1&modificationDate=1569656552720&api=v2)

![Settings](https://confluence.colonier.games/download/attachments/983095/Settings.png?version=1&modificationDate=1569656553163&api=v2)

![Modal](https://confluence.colonier.games/download/attachments/983095/Modal.png?version=1&modificationDate=1569656553023&api=v2)