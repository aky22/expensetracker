/**
 * Load all transaction from the database using the :transactionid param
 * The result is saved to res.locals.transaction
 */

const Schema = require('mongoose').Schema;
const db = require('../config/db');

const Transaction = db.model('Transaction', new Schema({
    date: Date,
    amount: Number,
    comment: String,
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}));

module.exports = Transaction;