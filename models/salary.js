const Schema = require('mongoose').Schema;
const db = require('../config/db');

const Salary = db.model('Salary', new Schema({
    amount: Number,
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}));

module.exports = Salary;