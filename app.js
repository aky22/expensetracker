const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');

app.set("view engine","ejs");
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.use('/public', express.static('public'));

app.use(
    session({
        secret: '229KJ2n2wfr6Vo7m'
    })
);

// Load routing
require('./route/index')(app);

app.use((err, req, res, next) => {
    res.end('Problem...');
    console.log(err);
});

app.listen(3000, function() {
    console.log('Hello :3000');
});