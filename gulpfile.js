'use strict';

var config = {};
config.docRoot = '.';


var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourceMaps = require('gulp-sourcemaps');

function onError(err) {
    console.log(err);
    this.emit('end');
}

gulp.task('sass', function () {
    return gulp.src(config.docRoot + '/scss/import.scss')
        .pipe(sourceMaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', onError))
        .pipe(concat('style.css'))
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(config.docRoot + '/public/css/'))
});