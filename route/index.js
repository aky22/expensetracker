const authMW = require('../middleware/auth/authMW');
const loginMW = require('../middleware/auth/loginMW');
const logoutMW = require('../middleware/auth/logoutMW');
const renderMW = require('../middleware/renderMW');

const delTransactionMW = require('../middleware/transactions/delTransactionMW');
const saveTransactionMW = require('../middleware/transactions/saveTransactionMW');
const getTransactionMW = require('../middleware/transactions/getTransactionMW');
const listTransactionsMW = require('../middleware/transactions/listTransactionsMW');
const returnJsonMW = require('../middleware/transactions/returnJsonMW');
const paymentMW = require('../middleware/transactions/paymentMW');

const delUserMW = require('../middleware/users/delUserMW');
const saveUserMW = require('../middleware/users/saveUserMW');
const getUserMW = require('../middleware/users/getUserMW');
const listUsersMW = require('../middleware/users/listUserMW');
const returnUserJsonMW = require('../middleware/users/returnUserJsonMW');

const getSettingMW = require('../middleware/settings/getSettingMW');
const saveSettingMW = require('../middleware/settings/saveSettingMW');

const TransactionModel = require('../models/transaction');
const UserModel = require('../models/user');
const SalaryModel = require('../models/salary');

module.exports = function(app) {
    const objRepo = {
        TransactionModel: TransactionModel,
        UserModel: UserModel,
        SalaryModel: SalaryModel
    };

    app.use(
        '/transaction/new',
        authMW(objRepo),
        saveTransactionMW(objRepo)
    );
    app.use(
        '/transaction/edit/:transactionid',
        authMW(objRepo),
        getTransactionMW(objRepo),
        saveTransactionMW(objRepo),
        renderMW(objRepo, 'transactionEditNew')
    );
    app.get(
        '/transaction/del/:transactionid',
        authMW(objRepo),
        getTransactionMW(objRepo),
        delTransactionMW(objRepo)
    );
    app.get(
        '/transactions',
        authMW(objRepo),
        listTransactionsMW(objRepo),
        renderMW(objRepo, 'index')
    );

    app.get(
        '/transaction/:transactionid',
        authMW(objRepo),
        getTransactionMW(objRepo),
        returnJsonMW(objRepo)
    );

    app.get(
        '/payment',
        authMW(objRepo),
        getSettingMW(objRepo),
        paymentMW(objRepo)
    );

    app.get(
        '/settings',
        authMW(objRepo),
        getSettingMW(objRepo),
        renderMW(objRepo, 'settings')
    );

    app.post(
        '/settings',
        authMW(objRepo),
        getSettingMW(objRepo),
        saveSettingMW(objRepo)
    );

    app.use(
        '/user/new',
        authMW(objRepo),
        saveUserMW(objRepo),
        renderMW(objRepo, 'userEditNew')
    );
    app.use(
        '/user/edit/:userid',
        authMW(objRepo),
        getUserMW(objRepo),
        saveUserMW(objRepo),
        renderMW(objRepo, 'userEditNew')
    );
    app.get(
        '/user/del/:userid',
        authMW(objRepo),
        getUserMW(objRepo),
        delUserMW(objRepo)
    );

    app.get(
        '/users',
        authMW(objRepo),
        listUsersMW(objRepo),
        renderMW(objRepo, 'users')
    );

    app.get(
        'user/:userid',
        authMW(objRepo),
        getUserMW(objRepo),
        returnUserJsonMW(objRepo)
    );

    app.use('/logout', logoutMW(objRepo));

    app.use('/', loginMW(objRepo), renderMW(objRepo, 'login'));
};