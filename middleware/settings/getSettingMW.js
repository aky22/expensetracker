/**
 * Load a transaction from the database using the :transactionid param
 * The result is saved to res.locals.transaction
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const SalaryModel = requireOption(objectrepository, 'SalaryModel');

    return function(req, res, next) {
        SalaryModel.findOne({}, (err, salary) => {
            if (err || !salary) {
                return next(err);
            }

            res.locals.salary = salary;
            return next();
        });
    };
};