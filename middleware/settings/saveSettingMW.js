/**
 * Using POST params update or save a transaction to the database
 * If res.locals.transaction is there, it's an update otherwise
 * this middleware creates an entity
 * Redirects to /main after success
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const SalaryModel = requireOption(objectrepository, 'SalaryModel');

    return function(req, res, next) {
        if (
            typeof req.body.amount === 'undefined'
        ) {
            return next();
        }

        if (typeof res.locals.salary === 'undefined') {
            res.locals.salary = new SalaryModel();
        }

        res.locals.salary.amount = req.body.amount;
        res.locals.salary.user_id = req.session.user_id;

        res.locals.salary.save(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/settings');
        });
    };
};