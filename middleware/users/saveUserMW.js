/**
 * Using POST params update or save a transaction to the database
 * If res.locals.transaction is there, it's an update otherwise
 * this middleware creates an entity
 * Redirects to /main after success
 */
const requireOption = require('../requireOption');
var CryptoJS = require("crypto-js");

module.exports = function(objectrepository) {
    const UserModel = requireOption(objectrepository, 'UserModel');

    return function(req, res, next) {
        if (
            typeof req.body.username === 'undefined' ||
            typeof req.body.password === 'undefined'
        ) {
            return next();
        }

        if (typeof res.locals.user === 'undefined') {
            res.locals.user = new UserModel();
        }

        res.locals.user.username = req.body.username;
        res.locals.user.password = CryptoJS.SHA3(req.body.password);

        res.locals.user.save(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/users');
        });
    };
};