/**
 * Using POST params update or save a transaction to the database
 * If res.locals.transaction is there, it's an update otherwise
 * this middleware creates an entity
 * Redirects to /main after success
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const UserModel = requireOption(objectrepository, 'UserModel');

    return function(req, res, next) {
        res.setHeader('Content-Type', 'application/json');
        return res.end(JSON.stringify(res.locals.user));
    };
};