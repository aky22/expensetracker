/**
 * Using POST params update or save a transaction to the database
 * If res.locals.transaction is there, it's an update otherwise
 * this middleware creates an entity
 * Redirects to /main after success
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const TransactionModel = requireOption(objectrepository, 'TransactionModel');

    return function(req, res, next) {
        if (
            typeof req.body.date === 'undefined' ||
            typeof req.body.amount === 'undefined' ||
            typeof req.body.comment === 'undefined'
        ) {
            return next();
        }

        if (typeof res.locals.transaction === 'undefined') {
            res.locals.transaction = new TransactionModel();
        }

        res.locals.transaction.date = req.body.date;
        res.locals.transaction.amount = req.body.amount;
        res.locals.transaction.comment = req.body.comment;
        res.locals.transaction.user_id = req.session.user_id;

        res.locals.transaction.save(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/transactions');
        });
    };
};