/**
 * Using POST params update or save a transaction to the database
 * If res.locals.transaction is there, it's an update otherwise
 * this middleware creates an entity
 * Redirects to /main after success
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const TransactionModel = requireOption(objectrepository, 'TransactionModel');

    return function(req, res, next) {
        res.locals.transaction = new TransactionModel();

        res.locals.transaction.date = new Date().toISOString().slice(0,10);
        res.locals.transaction.amount = res.locals.salary.amount;
        res.locals.transaction.comment = "Payment";
        res.locals.transaction.user_id = req.session.user_id;

        res.locals.transaction.save(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/transactions');
        });
    };
};