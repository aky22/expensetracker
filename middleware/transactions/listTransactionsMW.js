const requireOption = require('../requireOption');

/**
 *
 * @param objectrepository
 * @returns {Function}
 */
module.exports = function(objectrepository) {
    const TransactionModel = requireOption(objectrepository, 'TransactionModel');

    return function(req, res, next) {
        TransactionModel.find({user_id: req.session.user_id}, (err, transactions) => {
            if (err) {
                return next(err);
            }

            res.locals.transactions = transactions;
            res.locals.sumammount = 0;
            transactions.forEach((transaction) => {
               res.locals.sumammount += transaction.amount;
            });
            return next();
        });
    };
};