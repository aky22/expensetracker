/**
 * Removes a transaction from the database, the entity used here is: res.locals.transaction
 * Redirects to /main after delete
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    return function(req, res, next) {
        if (typeof res.locals.transaction === 'undefined') {
            return next();
        }

        res.locals.transaction.remove(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/transactions');
        });
    };
};