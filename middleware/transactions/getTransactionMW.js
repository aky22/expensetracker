/**
 * Load a transaction from the database using the :transactionid param
 * The result is saved to res.locals.transaction
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const TransactionModel = requireOption(objectrepository, 'TransactionModel');

    return function(req, res, next) {
        TransactionModel.findOne({ _id: req.params.transactionid }, (err, transaction) => {
            if (err || !transaction) {
                return next(err);
            }

            res.locals.transaction = transaction;
            return next();
        });
    };
};