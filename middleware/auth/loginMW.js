const requireOption = require('../requireOption');
var CryptoJS = require("crypto-js");

module.exports = function(objectrepository) {
    return function(req, res, next) {
        if (typeof req.body.password === 'undefined') {
            return next();
        }

        const UserModel = requireOption(objectrepository, 'UserModel');


        UserModel.findOne({username: req.body.username}, (err, user) => {

            if (user.password === CryptoJS.SHA3(req.body.password).toString() ) {
                req.session.user_id = user._id;
                return req.session.save(err => res.redirect('/transactions'));
            }

            if (err) {
                res.locals.error = 'Hibás jelszó!';
                return next();
            }

        });
    };
};